# Share-Service

---
## Installing
### Build image, create and start database container
```shell
docker-compose up -d --build
```

### Install the project dependencies
```shell
cd src && poetry install
```

### Spawn a shell within the virtual environment
```shell
poetry shell
```
---
## Jet-admin
### Migrate jet to enable it
```shell
python manage.py migrate jet
```
---
## Rosetta
### Compile messages for rosetta
```shell
django-admin compilemessages 
```
---
## Celery&Flower
### Run Celery
```shell
celery -A share_service worker -l INFO
celery -A share_service beat -l INFO
```
### Run flower
```shell
flower -A share_service --port=5555
```
---
## Tests
### For testing use
```shell
pytest
```

### Use the commands below to get test-coverage report
```shell
coverage run --source=. -m pytest
coverage report
```
---
## Utils
### export .env.local vars
```shell
export $(grep -v "^#" .env.local | xargs)
```
---
