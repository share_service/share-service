from django.contrib import admin

from core.models import Goal, GoalCategory, GoalParty, GoalPartyRequest, User, UuidToken

admin.site.register(User)
admin.site.register(Goal)
admin.site.register(GoalParty)
admin.site.register(GoalPartyRequest)
admin.site.register(GoalCategory)
admin.site.register(UuidToken)
