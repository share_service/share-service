from django.db.models import Choices
from django.utils.translation import gettext_lazy as _


class TokenEnum(Choices):
    EMAIL_VERIFICATION = _("EMAIL_VERIFICATION")
    FORGOT_PASSWORD = _("FORGOT_PASSWORD")
