from django.db import models
from django.utils.translation import gettext_lazy as _
from stdimage import StdImageField

from core.models.base import BaseModel


class GoalCategory(BaseModel):
    title = models.CharField(max_length=255)
    image = models.ImageField(upload_to="goal_categories")
    bg_image = StdImageField(upload_to="goal_categories")

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = "goal categories"


class Goal(BaseModel):
    title = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    image = StdImageField(upload_to="goal_images", variations={"thumbnail": {"width": 100, "height": 75}})
    bg_image = StdImageField(upload_to="goal_images")
    max_number_of_members = models.PositiveSmallIntegerField()
    category = models.ForeignKey("GoalCategory", on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.title


class GoalParty(BaseModel):
    goal = models.ForeignKey("Goal", on_delete=models.CASCADE)
    admin = models.ForeignKey("User", on_delete=models.CASCADE, related_name="admin_of_goal_party")
    members = models.ManyToManyField("User", blank=True)

    def __str__(self):
        return f"{_('Party for goal')} {self.goal}"

    class Meta:
        verbose_name_plural = _("goal parties")


class GoalPartyRequest(BaseModel):
    goal = models.ForeignKey("Goal", on_delete=models.CASCADE)
    user = models.ForeignKey("User", on_delete=models.CASCADE)

    def __str__(self):
        return f"{_('Request to party')} {self.goal} {_('from')} {self.user}"
