from django.conf import settings
from django.contrib.auth.models import AbstractUser, UserManager
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import gettext_lazy as _
from rest_framework.authtoken.models import Token
from stdimage import StdImageField

from share_service.tasks import process_photo_image


def image_processor(file_name, variations, storage):
    process_photo_image.delay(file_name, variations)
    return False


class CustomUserManager(UserManager):
    def create_superuser(self, email, password=None, **extra_fields):
        super().create_superuser(username=email, email=email, password=password)


class User(AbstractUser):
    email = models.EmailField(_("email address"), blank=True, unique=True)
    avatar = StdImageField(
        upload_to="avatars",
        blank=True,
        null=True,
        variations={"thumbnail": {"width": 100, "height": 75}},
        render_variations=image_processor,
    )
    is_email_verified = models.BooleanField(default=False)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []
    objects = CustomUserManager()

    def save(self, *args, **kwargs):
        if not self.pk:
            self.username = self.email
        super().save(*args, **kwargs)

    def verify_email(self):
        self.is_email_verified = True
        self.save()

    def __str__(self):
        return self.email


# creates unique token when user created
# перенести в save
@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
