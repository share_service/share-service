import datetime
import uuid

from django.db import models
from django.utils import timezone

from core.enums import TokenEnum
from share_service.settings import VERIFICATION_TOKEN_EXPIRATION_TIME


class UuidToken(models.Model):
    value = models.UUIDField()
    expiration_date = models.DateTimeField()
    user = models.ForeignKey("User", on_delete=models.CASCADE)
    token_type = models.CharField(max_length=55, choices=TokenEnum.choices, default="EMAIL_VERIFICATION")

    def save(self, *args, **kwargs):
        self.value = uuid.uuid4()
        self.expiration_date = timezone.now() + datetime.timedelta(seconds=VERIFICATION_TOKEN_EXPIRATION_TIME)
        super().save(*args, **kwargs)
