import django.contrib.auth.password_validation as validators
from django.contrib.auth.hashers import make_password  # noqa
from django.core import exceptions
from rest_framework import serializers
from rest_framework.authtoken.models import Token

from core.models import User, UuidToken
from core.serializers import UserSerializer  # noqa


class UserAuthOutputSerializer(serializers.ModelSerializer):
    # при наследовании от UserSerializer не выводится auth_token
    auth_token = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = (
            "id",
            "first_name",
            "last_name",
            "email",
            "is_email_verified",
            "avatar",
            "auth_token",
        )

    def get_auth_token(self, obj):
        token, created = Token.objects.get_or_create(user=obj)
        return token.key


class ConfirmEmail(serializers.ModelSerializer):
    class Meta:
        model = UuidToken
        fields = ("value",)


class SignUpSerializerResponse(serializers.ModelSerializer):
    token = serializers.CharField(max_length=40)

    class Meta:
        model = User
        fields = ["first_name", "last_name", "email", "password", "token"]


class SignUpSerializer(serializers.ModelSerializer):
    # password = serializers.CharField(style={"input_type": "password"}, write_only=True)

    class Meta:
        model = User
        fields = [
            "first_name",
            "last_name",
            "email",
            "password",
        ]

    def validate(self, data):
        user = User(**data)  # noqa
        password = data.get("password")
        if data.get("password") == data.get("email"):
            raise serializers.ValidationError("Password is the same as the email!")
        errors = dict()
        try:
            validators.validate_password(password=password)
        except exceptions.ValidationError as e:  # noqa
            errors["password"] = list(e.messages)
        if errors:
            raise serializers.ValidationError(errors)
        return super(SignUpSerializer, self).validate(data)

    def save(self):
        user = User(**self.validated_data)
        password = self.validated_data["password"]
        user.set_password(password)
        user.save()
        return user


class SignInSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            "email",
            "password",
        ]


class SignInSerializerResponse(serializers.ModelSerializer):
    token = serializers.CharField(max_length=40)

    class Meta:
        model = User
        fields = ["email", "token"]


class ForgotPasswordSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("email",)


class SetNewPasswordSerializer(serializers.ModelSerializer):
    token = serializers.UUIDField()

    class Meta:
        model = User
        fields = ("token", "password")
