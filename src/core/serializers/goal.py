from crum import get_current_user
from django.db.models import Q
from rest_framework import serializers

from core.models import Goal, GoalCategory, GoalParty, GoalPartyRequest
from core.serializers import UserSerializer


class GoalCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = GoalCategory
        fields = (
            "id",
            "title",
            "image",
            "bg_image",
        )


class GoalCompactSerializer(serializers.ModelSerializer):
    category = GoalCategorySerializer()

    class Meta:
        model = Goal
        fields = (
            "id",
            "title",
            "description",
            "image",
            "bg_image",
            "category",
        )


class GoalSerializer(serializers.ModelSerializer):
    category = GoalCategorySerializer()
    my_goal_party = serializers.SerializerMethodField()

    class Meta:
        model = Goal
        fields = ("id", "title", "description", "image", "bg_image", "category", "my_goal_party")

    def get_my_goal_party(self, obj):
        user = get_current_user()
        if user.is_authenticated:
            try:
                gp = GoalParty.objects.get(Q(admin=user) | Q(members__in=[user]), goal=obj)
                return {"id": gp.id}
            except GoalParty.DoesNotExist:
                return None


class GoalPartyRequestSerializer(serializers.ModelSerializer):
    goal = GoalCompactSerializer()
    user = UserSerializer()

    class Meta:
        model = GoalPartyRequest
        fields = (
            "goal",
            "user",
        )
