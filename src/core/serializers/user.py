from django.contrib.auth.models import AnonymousUser
from rest_framework import serializers
from rest_framework.exceptions import PermissionDenied

from core.models import User


class UserAvatarSerializer(serializers.ModelSerializer):
    avatar = serializers.ImageField()

    class Meta:
        model = User
        fields = ("avatar",)


class UserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(read_only=True)
    is_email_verified = serializers.BooleanField(read_only=True)

    class Meta:
        model = User
        fields = (
            "id",
            "first_name",
            "last_name",
            "email",
            "is_email_verified",
            "avatar",
        )

    def to_representation(self, obj):
        request = self.context.get("request")

        if isinstance(obj, AnonymousUser):
            raise PermissionDenied

        if obj.avatar:
            avatar = {
                "original": request.build_absolute_uri(obj.avatar.url),
                "thumbnail": request.build_absolute_uri(obj.avatar.thumbnail.url),
            }
        else:
            avatar = None

        fields = {
            "id": obj.id,
            "first_name": obj.first_name,
            "last_name": obj.last_name,
            "email": obj.email,
            "is_email_verified": obj.is_email_verified,
            "avatar": avatar,
        }

        return fields


class UserChangePasswordSerializer(serializers.ModelSerializer):
    old_password = serializers.CharField(style={"input_type": "password"}, write_only=True)
    new_password = serializers.CharField(style={"input_type": "password"}, write_only=True)

    class Meta:
        model = User
        fields = ("old_password", "new_password")
