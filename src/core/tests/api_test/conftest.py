import os

import pytest
from rest_framework.test import APIClient

from share_service.settings import MEDIA_ROOT


@pytest.fixture
def api_client():
    return APIClient()


@pytest.fixture(scope="session")
def media():
    path = MEDIA_ROOT + "/goal_categories/"
    os.makedirs(path)
    yield
    os.rmdir(path)
