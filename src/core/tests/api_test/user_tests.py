import os

import pytest
from django.utils.crypto import get_random_string
from rest_framework import status

from core.tests.factory_utils import UserFactory, create_temp_image

incorrect_passwords = ["1234567", "123456789", "qwertyuiop", "same_as_email"]

""" Testcase #1 - /api/user/ (success) --GET """


@pytest.mark.django_db
def test_api_user(api_client):
    dummy_user = UserFactory()
    api_client.force_authenticate(user=dummy_user)
    request = api_client.get(path="/api/user/")

    assert request.status_code == 200
    assert request.json()["email"] == dummy_user.email


""" Testcase #2 - /api/user/ (fail) --GET """


@pytest.mark.django_db
def test_api_user_f(api_client):
    request = api_client.get(path="/api/user/")

    assert request.status_code == 401


""" Testcase #3 - /api/user/{id} (success) --GET """


@pytest.mark.django_db
def test_api_userid(api_client):
    dummy_user = UserFactory()
    api_client.force_authenticate(user=dummy_user)
    request = api_client.get(path="/api/user/")
    id = request.json()["id"]
    request_id = api_client.get(path=f"/api/user/{id}/")

    assert request_id.status_code == 200
    assert dummy_user.pk == request_id.json()["id"]
    assert dummy_user.email == request_id.json()["email"]


""" Testcase #4 - /api/user/{id} (fail) --GET """


@pytest.mark.django_db
def test_api_userid_f(api_client):
    # trying to get user with fake id
    dummy_user = UserFactory()
    api_client.force_authenticate(user=dummy_user)
    request = api_client.get(path="/api/user/-1/")

    assert request.status_code == 404


""" Testcase #5 - /api/user/{id} (success) --PATCH """


@pytest.mark.django_db
def test_api_userid_patch(api_client):
    dummy_user = UserFactory()
    api_client.force_authenticate(user=dummy_user)
    request = api_client.get(path="/api/user/")
    id = request.json()["id"]
    data = {"first_name": "Test", "last_name": "Test2"}
    request_patch = api_client.patch(path=f"/api/user/{id}/", data=data)

    assert request_patch.status_code == 200
    assert request_patch.json()["first_name"] == data["first_name"]
    assert request_patch.json()["last_name"] == data["last_name"]


""" Testcase #6 - /api/user/{id} (fail) --PATCH """


@pytest.mark.django_db
def test_api_userid_patch_f(api_client):
    # creating two users
    dummy_victim = UserFactory()
    dummy_requester = UserFactory()
    # login as a victim
    api_client.force_authenticate(user=dummy_victim)
    # requesting victim's id
    request = api_client.get(path="/api/user/")
    id = request.json()["id"]
    api_client.logout()
    # login as a requester
    api_client.force_authenticate(user=dummy_requester)
    data = {"first_name": "Test", "last_name": "Test2"}
    # trying to patch a victim's data
    request_patch = api_client.patch(path=f"/api/user/{id}/", data=data)

    assert request_patch.status_code == 403


""" Testcase #7 - /api/user/avatar/ (success) --POST """


@pytest.mark.django_db
def test_api_user_avatar_post(api_client):
    dummy_user = UserFactory()
    api_client.force_authenticate(user=dummy_user)
    avatar_file = create_temp_image(".jpg")
    data = {"avatar": avatar_file}
    request = api_client.post(path="/api/user/avatar/", data=data, format="multipart")

    assert request.status_code == 200

    avatars = [file for file in os.listdir(os.path.join(os.getcwd(), "mediafiles", "avatars"))]

    assert request.json()["avatar"]["original"].split("/")[-1] in avatars
    assert request.json()["avatar"]["thumbnail"].split("/")[-1] in avatars


""" Testcase #8 - /api/user/avatar/ (fail) --POST """


@pytest.mark.django_db
def test_api_user_avatar_post_f(api_client):
    avatar_file = create_temp_image(".jpg")
    data = {"avatar": avatar_file}
    request = api_client.post(path="/api/user/avatar/", data=data, format="multipart")

    assert request.status_code == 401

    avatars = [file for file in os.listdir(os.path.join(os.getcwd(), "mediafiles", "avatars"))]
    hypothetic_thumbnail_filename = str(avatar_file.name.split(".")[1]) + ".thumbnail.jpg"

    assert avatar_file.name not in avatars
    assert hypothetic_thumbnail_filename not in avatars


""" Testcase #9 - /api/user/{id}/change_password/ (success) --PATCH """


@pytest.mark.django_db
def test_user_change_password_success(api_client):
    user = UserFactory()
    password = "Test"
    user.set_password(password)
    user.save()

    api_client.force_authenticate(user=user)
    request = api_client.get(path="/api/user/")
    id = request.json()["id"]

    old_password = "Test"
    new_password = get_random_string()

    assert user.check_password(old_password)
    assert not user.check_password(new_password)

    data = {"old_password": old_password, "new_password": new_password}
    r = api_client.patch(path=f"/api/user/{id}/change_password/", data=data)

    user.refresh_from_db()
    assert r.status_code == status.HTTP_200_OK
    assert user.check_password(new_password)


""" Testcase #10 - /api/user/{id}/change_password/ (fail) --PATCH """


@pytest.mark.parametrize("password", incorrect_passwords)
@pytest.mark.django_db
def test_user_change_fail_incorrect_new_password(password, api_client):
    user = UserFactory.create()
    password = "Test"
    user.set_password(password)
    user.save()

    api_client.force_authenticate(user=user)
    request = api_client.get(path="/api/user/")
    id = request.json()["id"]

    old_password = "Test"
    assert user.check_password(old_password)

    r = api_client.patch(
        path=f"/api/user/{id}/change_password/",
        data={"old_password": old_password, "new_password": user.email if password == "same_as_email" else password},
    )

    assert r.status_code == status.HTTP_400_BAD_REQUEST


""" Testcase #11 - /api/user/{id}/change_password/ (fail) --PATCH """


@pytest.mark.django_db
def test_user_change_fail_incorrect_old_password(api_client):
    user = UserFactory.create()
    password = "Test"
    user.set_password(password)
    user.save()

    api_client.force_authenticate(user=user)
    request = api_client.get(path="/api/user/")
    id = request.json()["id"]

    r = api_client.patch(
        path=f"/api/user/{id}/change_password/",
        data={"old_password": get_random_string(), "new_password": get_random_string()},
    )

    assert r.status_code == status.HTTP_400_BAD_REQUEST
