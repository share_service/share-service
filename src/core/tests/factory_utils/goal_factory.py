import random

import factory

from core.models import Goal, GoalCategory
from core.tests.factory_utils import create_temp_image
from share_service.settings import MEDIA_ROOT


class GoalCategoryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = GoalCategory

    title = "Goal Category " + str(random.randint(1, 20))
    image = factory.LazyAttribute(lambda a: f"{create_temp_image('.jpg', path=MEDIA_ROOT+'/goal_categories/')}")


class GoalFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Goal

    title = "Goal " + str(random.randint(1, 20))
    max_number_of_members = random.randint(1, 10)
    category = factory.SubFactory(GoalCategoryFactory)
