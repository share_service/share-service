from tempfile import NamedTemporaryFile

from PIL import Image


def create_temp_image(avatar_suffix, path=None):
    image = Image.new("RGB", (100, 100))
    avatar_file = NamedTemporaryFile(suffix=avatar_suffix, dir=path)
    image.save(avatar_file)
    avatar_file.seek(0)
    return avatar_file
