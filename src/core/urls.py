from django.conf import settings
from django.urls import path
from drf_yasg import openapi
from drf_yasg.generators import OpenAPISchemaGenerator
from drf_yasg.views import get_schema_view
from rest_framework.permissions import IsAuthenticated
from rest_framework.routers import SimpleRouter

from .views import AuthViewSet, GoalViewSet, UserViewSet


def get_schema_view_description(commit: str, branch: str) -> str:
    return f"Commit: {commit}\nBranch: {branch}"


class CustomSchemaGenerator(OpenAPISchemaGenerator):
    def get_schema(self, request=None, public=False):
        schema = super().get_schema(request, public)
        schema.schemes = ["https", "http"]
        if settings.ENVIRONMENT == "local":
            schema.schemes.reverse()
        return schema


schema_view = get_schema_view(
    openapi.Info(
        title="Share Service Api",
        default_version="0.1",
        description=get_schema_view_description(settings.CI_COMMIT_SHA, settings.CI_COMMIT_REF_NAME,),
    ),
    public=True,
    generator_class=CustomSchemaGenerator,
    permission_classes=(IsAuthenticated,),
)


app_name = "core"

router = SimpleRouter()
router.register("user", UserViewSet, basename="user")
router.register("auth", AuthViewSet, basename="auth")
router.register("goal", GoalViewSet, basename="goal")


urlpatterns = [
    path("swagger/", schema_view.with_ui("swagger", cache_timeout=0), name="schema-swagger",),
    path("redoc/", schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc",),
]

urlpatterns += router.urls
