from .auth import AuthViewSet
from .goal import GoalViewSet
from .user import UserViewSet

__all__ = [
    UserViewSet,
    AuthViewSet,
    GoalViewSet,
]
