from django.utils.translation import gettext_lazy as _
from drf_yasg import openapi
from drf_yasg.utils import no_body, swagger_auto_schema
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import IsAuthenticated  # noqa
from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet

from core.models import Goal, GoalCategory, GoalParty, GoalPartyRequest
from core.serializers import (
    GoalCategorySerializer,
    GoalPartyRequestSerializer,
    GoalSerializer,
)

category_id_param = openapi.Parameter("category_id", openapi.IN_QUERY, description="pk", type=openapi.TYPE_INTEGER)


class GoalViewSet(ReadOnlyModelViewSet):
    model = Goal
    serializer_class = GoalSerializer
    queryset = Goal.objects.all()
    permission_classes_by_action = {"join": [IsAuthenticated]}

    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_classes_by_action[self.action]]
        except KeyError:
            return super().get_permissions()

    def get_queryset(self):
        self.category_id = self.request.query_params.get("category_id")
        try:
            category = GoalCategory.objects.get(pk=self.category_id)
        except GoalCategory.DoesNotExist:
            return Goal.objects.all()
        goals_query = Goal.objects.filter(category=category)
        return goals_query

    @swagger_auto_schema(manual_parameters=[category_id_param], responses={200: GoalSerializer()})
    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(request_body=no_body, responses={200: GoalPartyRequestSerializer()})
    @action(methods=["POST"], detail=True)
    def join(self, request, pk):
        try:
            goal = Goal.objects.get(id=pk)
        except Goal.DoesNotExist:
            raise ValidationError(_("Goal Does Not Exist"))

        goal_party = GoalParty.objects.filter(goal=goal)
        if any(
            [
                goal_party.filter(admin=request.user).count() != 0,
                goal_party.filter(members__in=[request.user]).count() != 0,
            ]
        ):
            raise ValidationError(_("You are already attending a party for this goal"))

        if GoalPartyRequest.objects.filter(goal=goal, user=request.user).count() != 0:
            raise ValidationError(_("You have already submitted a request to join this goal"))

        gpr = GoalPartyRequest.objects.create(user=request.user, goal=goal)
        return Response(GoalPartyRequestSerializer(gpr).data)

    @swagger_auto_schema(
        operation_summary="get all goal categories", request_body=no_body, responses={200: GoalCategorySerializer()},
    )
    @action(methods=["GET"], detail=False)
    def categories(self, request):
        queryset = GoalCategory.objects.all()
        serializer = GoalCategorySerializer(queryset, many=True, context={"request": request})
        return Response(serializer.data, status=status.HTTP_200_OK)
