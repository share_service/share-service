import os

from celery import Celery
from celery.schedules import crontab  # noqa

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "share_service.settings")

app = Celery("share_service")

app.config_from_object("django.conf:settings", namespace="CELERY")

app.conf.beat_schedule = {"test": {"task": "share_service.tasks.test", "schedule": 1 * 60, "args": ("test",),}}

app.autodiscover_tasks()


@app.task(bind=True)
def debug_task(self):
    print(f"Request: {self.request!r}")
