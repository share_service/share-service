from celery import shared_task
from django.apps import apps
from django.core.mail import send_mail
from stdimage.utils import render_variations

from share_service.settings import EMAIL_HOST_USER


@shared_task
def process_photo_image(file_name, variations):
    render_variations(file_name, variations, replace=True)
    obj = apps.get_model("core", "User").objects.get(avatar=file_name)
    obj.processed = True
    obj.save()


@shared_task
def send_email(subject, html_message, recipient_list):
    send_mail(
        subject=subject,
        message=None,
        html_message=html_message,
        from_email=EMAIL_HOST_USER,
        recipient_list=recipient_list,
    )


@shared_task
def test(test):
    print(f"{test} succeed!")
