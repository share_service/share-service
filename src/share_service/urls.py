from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path

from share_service import settings

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/", include("core.urls", namespace="api")),
    path("jet/", include("jet.urls", "jet")),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if "rosetta" in settings.INSTALLED_APPS:
    urlpatterns += [path("rosetta/", include("rosetta.urls"))]
